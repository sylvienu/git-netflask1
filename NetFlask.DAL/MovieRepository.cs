﻿using NetFlask.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.Database;

namespace NetFlask.DAL
{
    public class MovieRepository
    {
        static Random rnd = new Random();
        private Connection _oconn;
        private string _cnstr = @"Data Source=WAD-6\ADMINSQL;Initial Catalog=NetFlask;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public MovieRepository()
        {
            _oconn = new Connection(_cnstr);
        }
        public List<string> getFirstSix()
        {
            IEnumerable<string> images = new List<string>();
            Command cmd = new Command("SELECT TOP 6 Picture FROM MOVIE");
            images = _oconn.ExecuteReader<string>(cmd, d=>d["Picture"].ToString());

            return images.ToList();
        }

        //à relire, code Michael
        //public List<string> getFromTitle(string search)
        //{
        //    IEnumerable<string> images = new List<string>();
        //    Command cmd = new Command($"SELECT * FROM MOVIE where title like '%{search}%'");
        //    images = _oconn.ExecuteReader<string>(cmd, d => d["Picture"].ToString());

        //    return images.ToList();
        //}

        // maintenant plus proprement avec Mapping et "Arrow function"
        public List<Movie> getFromTitle(string search)
        {
            IEnumerable<Movie> images = new List<Movie>();
            Command cmd = new Command($"SELECT * FROM MOVIE where title like '%{search}%'");
            images = _oconn.ExecuteReader<Movie>(cmd,
                d =>
                new Movie()
                {
                    IdMovie = (int)d["idMovie"],
                    Picture = d["Picture"].ToString(),
                    Title = d["Title"].ToString(),
                    Duration = (int)d["Duration"],
                    Summary = d["Summary"].ToString(),
                    ReleaseDate = (DateTime)d["ReleaseDate"]
                    // TODO : Mapping des autres colonnes
                }
                
                );

            return images.ToList();
        }

        public List<string> getLastFour()
        {
            IEnumerable<string> images = new List<string>();
            Command cmd = new Command("SELECT TOP 4 Picture FROM MOVIE ORDER BY IdMovie DESC");
            images = _oconn.ExecuteReader<string>(cmd, d => d["Picture"].ToString());

            return images.ToList();
        }

        public List<string> getRandFour()
        {
            //int randInt = new Random().Next(0, 994);
            int myRandom =  rnd.Next(0,994);
            IEnumerable<string> images = new List<string>();
            Command cmd = new Command($"SELECT Picture FROM MOVIE ORDER BY IdMovie OFFSET {myRandom} ROWS Fetch NEXT 6 rows only");
            images = _oconn.ExecuteReader<string>(cmd, d => d["Picture"].ToString());

            return images.ToList();
        }

        //ESSAI
        public List<Movie> getFiftyTrailers()
        {
            IEnumerable<Movie> trailers = new List<Movie>();
            Command cmd = new Command($"SELECT TOP 20 * FROM MOVIE");
            trailers = _oconn.ExecuteReader<Movie>(cmd,
                d =>
                new Movie()
                {
                    IdMovie = (int)d["idMovie"],
                    Picture = d["Picture"].ToString(),
                    Title = d["Title"].ToString(),
                    Trailer = d["Trailer"].ToString(),
                }

                );

            return trailers.ToList();
        }

        public List<Movie> getTenAccordingPage(int id)
        {
            int debut = (id - 1) * 10;
            IEnumerable<Movie> tenOfThePage = new List<Movie>();
            Command cmd = new Command($"SELECT Picture FROM MOVIE ORDER BY IdMovie OFFSET {debut} ROWS Fetch NEXT 10 rows only");
            tenOfThePage = _oconn.ExecuteReader<Movie>(cmd,
               d =>
               new Movie()
               {
                   IdMovie = (int)d["idMovie"],
                   Picture = d["Picture"].ToString(),
                   Title = d["Title"].ToString(),
                   Trailer = d["Trailer"].ToString(),
               }

               );

            return tenOfThePage.ToList();
        }
    }
}
