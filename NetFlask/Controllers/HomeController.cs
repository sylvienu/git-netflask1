﻿using NetFlask.DAL;
using NetFlask.Entities;
using NetFlask.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetFlask.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Current = "Index";            

            return View(new HomeViewModel());
        }

        [HttpPost]
        public ActionResult SimpleSearch(string txtSearch)
        {
            ViewBag.Search = txtSearch;
            ViewBag.DropReview = false;
            //ViewBag.Current = "Index";
            //return View(new HomeViewModel());
            //à relire, code Michael
            MovieRepository rm = new MovieRepository();
            //Mapping
            List<Movie> ModelDb = rm.getFromTitle(txtSearch);
            //return View("resultat", rm.getFromTitle(txtSearch));

            //Transférer info
            List<MovieInfo> lm = new List<MovieInfo>();
            foreach (Movie item in ModelDb)
            {
                lm.Add(new MovieInfo() {
                    Title = item.Title,
                    Picture = item.Picture
                });
            }

            //ESSAI 1 idem que Review
            //List<MovieReview> moviesSearched = new List<MovieReview>();
            //for (int i = 0; i < lm.Count; i++)
            //{
            //    moviesSearched.Add(new MovieReview()
            //    {
            //        Picture = lm[i].Picture,
            //        Title = lm[i].Title
            //    });
            //}

            //ESSAI 2
            List<MovieReview> moviesSearched = new List<MovieReview>();
            foreach (Movie item in ModelDb)
            {
                moviesSearched.Add(new MovieReview()
                {
                    Picture = item.Picture,
                    Title = item.Title,
                    Summary = item.Summary,
                    DateReview = (DateTime)item.ReleaseDate
                });
            }

            //return View("resultat", lm);
            return View("resultat", moviesSearched);
        }

        [HttpGet]
        public ViewResult Videos()
        {
            ViewBag.Current = "Videos";

            // Mapping
            MovieRepository rm = new MovieRepository();
            List<Movie> ModelDb = rm.getFiftyTrailers();

            List<MovieInfo> trailersToDisplay = new List<MovieInfo>();
            foreach (Movie item in ModelDb)
            {
                trailersToDisplay.Add(new MovieInfo()
                {
                    Id = item.IdMovie,
                    Picture = item.Picture,
                    Title = item.Title,
                    Video = (item.Trailer).Replace("watch?v=", "embed/"),
                });
            }

            return View("Videos", trailersToDisplay);

        }

        public ViewResult Pagination(int id)
        {
            MovieRepository rm = new MovieRepository();
            List<Movie> ModelDb = rm.getTenAccordingPage(id);

            List<MovieInfo> myTenToDisplay = new List<MovieInfo>();
            foreach (Movie item in ModelDb)
            {
                myTenToDisplay.Add(new MovieInfo()
                {
                    Id = item.IdMovie,
                    Picture = item.Picture,
                    Title = item.Title,
                    Video = (item.Trailer).Replace("watch?v=", "embed/"),
                });
            }

            return View("Videos", myTenToDisplay);
        }

        public ViewResult Reviews()
        {
            List<MovieReview> movrev = new List<MovieReview>();
            movrev = FakeDb.LoadData();
            //string CastString = convertToString(movrev.Cast);
            //ViewBag.CastString = CastString;
            //BERK BERK BERK BERK!!!!!!!!!!!!!! 
            return View(movrev);
        }

        private string convertToString(List<string> casts)
        {if (casts.Count() == 0) return "";
            string retour = "";
            foreach (string item in casts)
            {
                retour += item + ",";

            }
            return retour.Substring(0, retour.Length - 1);
        }
    }
}