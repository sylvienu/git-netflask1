  using System;
    using System.Collections.Generic;
    
namespace NetFlask.Entities
{
   
    public class Director
    { 
        private int _idDirector ;
        private string _firstName ;
        private string _lastName ;
    
          private   IEnumerable<Movie> _movies ;

        public int IdDirector
        {
            get
            {
                return _idDirector;
            }

            set
            {
                _idDirector = value;
            }
        }

        public string FirstName
        {
            get
            {
                return _firstName;
            }

            set
            {
                _firstName = value;
            }
        }

        public string LastName
        {
            get
            {
                return _lastName;
            }

            set
            {
                _lastName = value;
            }
        }

        public IEnumerable<Movie> Movies
        {
            get
            {
                return _movies;
            }

            set
            {
                _movies = value;
            }
        }
    }
}
